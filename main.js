import './styles/style.css';
import './styles/normalize.css';

// eslint-disable-next-line import/extensions
import SectionCreator from './join-us-section.js';

function init() {
    const sectionCreator = new SectionCreator('standard');

    // Create standard program
    const standardProgram = sectionCreator.create();
    standardProgram.render();
}

window.addEventListener('load', init);
