const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

export default function isValidEmail(email) {
    // Extract the domain from the email address
    const [, emailDomain] = email.split('@');

    // Check if the domain is in the valid email addresses array
    return VALID_EMAIL_ENDINGS.includes(emailDomain);
}
