module.exports = {
    env: {
        browser: true,
        es2021: true,
    },
    extends: 'airbnb',
    overrides: [
        {
            env: {
                node: true,
            },
            files: ['.eslintrc.{js,cjs}'],
            parserOptions: {
                sourceType: 'script',
            },
        },
    ],
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
    },
    rules: {
        'max-classes-per-file': ['error', 3],
        indent: ['error', 4],
        'max-len': ['error', { code: 120 }],
        'import/extensions': ['error', 'ignorePackages', { js: 'always' }],
        'consistent-return': 'error',
    },
};
