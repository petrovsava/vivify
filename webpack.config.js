/* eslint-disable import/no-extraneous-dependencies */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { HotModuleReplacementPlugin } = require('webpack');
const TerserWebpackPlugin = require('terser-webpack-plugin');

module.exports = (env, argv) => {
    const isProduction = argv.mode === 'production';

    return {
        entry: './main.js',
        output: {
            filename: 'bundle.js',
            path: path.resolve(__dirname, 'dist'),
        },
        devServer: {
            static: {
                directory: path.join(__dirname, 'dist'),
            },
            hot: true,
            port: 3000,
            open: true,
        },
        optimization: {
            minimize: isProduction,
            minimizer: isProduction ? [new TerserWebpackPlugin()] : [],
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: './index.html',
            }),
            new CopyWebpackPlugin({
                patterns: [{ from: 'assets', to: 'assets' }],
            }),
            new HotModuleReplacementPlugin(),
        ],
        module: {
            rules: [
                {
                    test: /\.js$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env'],
                        },
                    },
                },
                {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader'],
                },
                {
                    test: /\.(png|svg|jpg|gif)$/,
                    use: ['url-loader'],
                },
            ],
        },
    };
};
