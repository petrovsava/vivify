import isValidEmail from "./email-validator.js";

class Section {
  constructor(text, buttonText, buttonUnsubText) {
    this.text = text;
    this.buttonText = buttonText;
    this.buttonUnsubText = buttonUnsubText;
  }

  render() {
    const main = document.getElementById("app-container");

    this.sectionElement = document.createElement("section");
    this.sectionElement.className = "app-section-join-our-program";
    main.appendChild(this.sectionElement);

    const footer = document.querySelector(".app-footer");
    main.insertBefore(this.sectionElement, footer);

    const joinOurProgram = document.createElement("div");
    joinOurProgram.className = "join-our-program";
    this.sectionElement.appendChild(joinOurProgram);

    const joinTitle = document.createElement("h2");
    joinTitle.className = "join-title";
    joinTitle.textContent = this.text;
    joinOurProgram.appendChild(joinTitle);

    const appSubtitle = document.createElement("h3");
    appSubtitle.className = "app-subtitle";
    appSubtitle.textContent;
    ("Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.");
    joinOurProgram.appendChild(appSubtitle);

    const form = document.createElement("form");
    form.className = "join-input-button";
    this.sectionElement.appendChild(form);

    const storedEmail = localStorage.getItem("storedEmail");

    if (storedEmail) {
      const buttonUnsub = document.createElement("buttonUnsub");
      buttonUnsub.className =
        "app-section__button app-section__button--subscribe";
      buttonUnsub.id = "sub-btnUnsub";
      buttonUnsub.textContent = this.buttonUnsubText;
      form.appendChild(buttonUnsub);

      buttonUnsub.addEventListener("click", (event) => {
        event.preventDefault();

        localStorage.removeItem("storedEmail");
      });
    } else {
      const input = document.createElement("input");
      input.className = "input";
      input.id = "input";
      input.type = "email";
      input.placeholder = "Email";

      const button = document.createElement("button");
      button.className = "app-section__button app-section__button--subscribe";
      button.id = "sub-btn";
      button.textContent = this.buttonText;
      form.appendChild(button);

      button.addEventListener("click", (event) => {
        event.preventDefault();
        if (isValidEmail(input.value)) {
          localStorage.setItem("storedEmail", input.value);
        }
      });

      form.appendChild(input);
    }
  }
}

class SectionCreator {
  constructor(type) {
    this.type = type;
  }

  create() {
    switch (this.type) {
      case "standard":
        return new Section("Join Our Program", "SUBSCRIBE", "UNSUBSCRIBE");
      case "advanced":
        return new Section(
          "Join Our Advanced Program",
          "Subscribe to Advanced Program",
          "Unsubscribe from Advanced Program"
        );
      default:
        throw new Error("Invalid section type");
    }
  }
}

export default SectionCreator;
